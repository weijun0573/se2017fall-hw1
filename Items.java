import java.io.*;
import java.util.*;

public class Items{

	public ArrayList<Item> items;

	public Items(){
		items = new ArrayList<Item>();
	}

	public void add(Item item){
		items.add(item);
	}	
	
	public Item getItem(String ItemID){
		Item ri = null;		
		for (Item i: items){
			if (i.ID == ItemID)
				ri = i;
		}
		return ri;
	}

}
