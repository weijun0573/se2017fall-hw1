import static org.junit.Assert.*;
import org.junit.Test;

public class ItemsTest{

    //test add item to Items
    @Test
    public void evaluatesAdd(){
	Items Warehouse = new Items();
	Item IT00001 = new Item ("IT00001", "soap", 5.43);
	Item IT00002 = new Item ("IT00002", "dish", 6.18);
	Warehouse.add(IT00001);
	assertEquals(true, Warehouse.items.contains(IT00001));
	assertEquals(false, Warehouse.items.contains(IT00002));
	Warehouse.add(IT00002);
	assertEquals(true, Warehouse.items.contains(IT00002));
    }

    //test getItem
    @Test
    public void evaluateGetItem(){
	Items Warehouse = new Items();
	Item IT00001 = new Item ("IT00001", "soap", 5.43);
	Item IT00002 = new Item ("IT00002", "dish", 6.18);
	Warehouse.add(IT00001);
	Warehouse.add(IT00002);
	assertEquals(IT00001, Warehouse.getItem("IT00001"));
	assertEquals(IT00002, Warehouse.getItem("IT00002"));
	assertEquals(null, Warehouse.getItem("IT00003"));
    }



}
