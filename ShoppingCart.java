import java.util.ArrayList;
import java.lang.*;

public class ShoppingCart{
	
    double delta = 0.00001;    
    private float itemDiscount10 = 0.1f;
    private float itemDiscount5 = 0.05f;
    private float tax = 0.045f;
    private float mDiscount = 0.1f;

    public Items inventory;

    //empty constructor
    public ShoppingCart(){}


    //constructor 
    public ShoppingCart(Items Storage){
	inventory = Storage;
    }

    //calculate item discount. if item number is more than 50, set it to -1.
    public float itemDiscount(ArrayList<String> ProductIDs){
	int n = ProductIDs.size();
	if (n > 50)
		return -1;
	else if (n >= 10 ) 
		return itemDiscount10;
	else if (n > 5) 
		return itemDiscount5;	
	else 
		return 0;
    }

    //calculate membership discount. if membership is wrong, set it to -1.
    public float memberDiscount(Customer customer){
	if (customer.membership == 0) 
		return 0;
	else if (customer.membership == -1)
		return -1;
	else 
		return mDiscount;
    }

    //calculate tax rate. if tax exempt type is wrong, set it to -1. 
    public float taxRate(Customer customer){
	if (customer.taxType == 0) 
		return tax;
	else if (customer.taxType == -1)
		return -1;
	else 
		return 0;
    }

    //count how may items cannot be found in inventory (return null), and print IDs of these items.
    public int nullItem (ArrayList<String> ProductIDs){
	int nullItem = 0;
	ArrayList<String> nullItemIDs = new ArrayList<String>();
	for (String s : ProductIDs){
		Item item = inventory.getItem(s);
		if (item == null) {
			nullItem ++;				//count items that cannot be found in inventory
			nullItemIDs.add(s);			//record these items
		}
	}	

	if (nullItem > 0){
		System.out.println("There is(are) " + nullItem + " item(s) that cannot be found in inventory.");
		System.out.print("These items are: ");
		for (String ns : nullItemIDs)
			System.out.print(ns + "; ");
			System.out.println();	
	}
	else
		System.out.println("All items are found in the inventory");
	return nullItem;
    }

    //Examine if there is any wrong input of price (any price is -1)
    public int wrongPriceItem (ArrayList<String> ProductIDs){
	int n = 0;
	ArrayList<String> wrongPriceIDs = new ArrayList<String>();
	for (String s : ProductIDs){
		Item item = inventory.getItem(s);
		if (item != null && (item.getPrice() < 0)) {
			n ++;				//count items price < 0
			wrongPriceIDs.add(s);		//record these items
		}
	}	

	if (n > 0){
		System.out.println("There is(are) " + n + " item(s) whose prices is(are) wrong.");
		System.out.print("These items are: ");
		for (String ns : wrongPriceIDs)
			System.out.print(ns + "; ");
			System.out.println();	
	}
	return n;
    }  


    //calculate net purchase price (no rounding)
    public float netPurchasePrice(ArrayList<String> ProductIDs){
	float netPurchasePrice = 0f;	
	for (String s : ProductIDs){
		Item item = inventory.getItem(s);
                if (item != null) 			
			netPurchasePrice += inventory.getItem(s).getPrice();
		}	
	return netPurchasePrice;
    }	


    //return an array of price including netPurchasePrice, netDiscount, taxAmount, netTotalDue.
    public float[] calcPurchasePrice(ArrayList<String> ProductIDs, Customer customer){
	
	float[] price = new float[4];	
	
	float itemDiscount = itemDiscount(ProductIDs);
	float membershipDiscount = memberDiscount(customer);
	float taxRate = taxRate(customer);

	float netPurchasePrice = 0;
	float discount;
	float netDiscount;
	float taxAmount;
	float netTotalDue;
	
	int nullItem;
	

	//if there are more than 50 items in this shopping cart, print out error massage and return -1;
	if (Math.abs(itemDiscount +1) < delta) {
		System.out.println("There are too many products in this shopping cart. Please re-set cart.");
		price[0] = -1; price [1] = -1; price [2] = -1; price [3] = -1;
	}
	
	//if membership is wrong, print out error massage and return -1;
	else if (Math.abs(membershipDiscount + 1) < delta) {
		System.out.println("The membership of this customer is wrong. Please re-set membership.");
		price[0] = -1; price [1] = -1; price [2] = -1; price [3] = -1;
	}


	//if tax type is wrong, print out error massage and return -1;
	else if (Math.abs(taxRate + 1) < delta) {
		System.out.println("The tax type of this customer is wrong. Please re-set tax type.");
		price[0] = -1; price [1] = -1; price [2] = -1; price [3] = -1;
	}
	
	else if (wrongPriceItem(ProductIDs) != 0) {
		System.out.println("There are some product their prices are wrong.");
		price[0] = -1; price [1] = -1; price [2] = -1; price [3] = -1;
	}

	else {
		nullItem = nullItem (ProductIDs);
		netPurchasePrice = netPurchasePrice (ProductIDs); 

		//total discount
		discount = itemDiscount + membershipDiscount;

		//calculate price before tax
		float priceBeforeTax = netPurchasePrice * (float)(1 - discount);

		//round these prices(rounding to integer after converting cents and then * 0.01		
		netPurchasePrice = rounding (netPurchasePrice);
		priceBeforeTax = rounding (priceBeforeTax);

		//calculate netDiscount
		netDiscount = netPurchasePrice - priceBeforeTax;

		
		//calculate tax
		taxAmount = priceBeforeTax * taxRate;
		//tax rounding
		taxAmount = rounding(taxAmount);
		//calculate total due
		netTotalDue = priceBeforeTax + taxAmount;

		System.out.println("Summary:");				
		System.out.println("You have total " + ProductIDs.size() + " items in this shopping cart.");
		System.out.println(nullItem + " of them cannot be found in the inventory.");		
		
		System.out.println("The price before discount is $" + String.format("%.2f", netPurchasePrice));
		System.out.println("You got " + (int)(itemDiscount*100) + "% from item number.");
		System.out.println("You got " + (int)(membershipDiscount*100) + "% from membership.");
		System.out.println("You saved total $" + String.format("%.2f",netDiscount));
		System.out.println("Tax is $" + String.format("%.2f",taxAmount));
		System.out.println("You have to pay $" + String.format("%.2f",netTotalDue));
		
		price[0] = netPurchasePrice;
		price[1] = netDiscount;
		price[2] = taxAmount;
		price[3] = netTotalDue;	

	}

	return price;

    }
    
    //converting number to cents and then round it it nearst integer. return int number multiplied by 0.01 
    public float rounding(float n){
	int cents = Math.round(n*100);
	return cents*0.01f;
    } 



    



}
