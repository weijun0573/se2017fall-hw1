import java.io.*;
import java.util.ArrayList;

public class Main{


	public static void main(String[] args){
	
	System.out.println("Customer information test:");	
	
	Customer One = new Customer ("Jun Wei", 0, 0);
	Customer Two = new Customer();
	Customer Three = new Customer("Henderson Wei", 3, 1);
        
        Two.setName("Henry Wei");
	Two.setMembership(1);
	Two.setTaxType (0);
	
	One.Info();
	Two.Info();
	Three.Info();

	System.out.println();
	System.out.println();
	System.out.println("Item information test:");

	Item IT00001 = new Item ("IT00001", "soap", 1.01);
	Item IT00002 = new Item ("IT00002", "wine", 7.49);
	System.out.println("item is " + IT00001.name + " and its price is " + IT00001.price);

	IT00001.setName ("dish");
	IT00001.setPrice (4.25);
	String s = IT00001.getName();
	double p = IT00001.getPrice();
	System.out.println("After change, this name of this item is " + s + " and its price is " + p);
	

	System.out.println();
	System.out.println();
	System.out.println("Items(inventroy) information test:");
	
	Item IT00003 = new Item ("IT00003", "apple", 3.56);
	Item IT00004 = new Item ("IT00004", "banana", 0.45);
	Item IT00005 = new Item ("IT00005", "cherry", 49.33);
	Item IT00006 = new Item ("IT00006", "durian", 4.53);
	Item IT00007 = new Item ("IT00007", "eggplant", 7.29);
	Item IT00008 = new Item ("IT00008", "fig", 18.34);	


	Items Warehouse = new Items();
	Warehouse.add (IT00001);
	Warehouse.add (IT00002);
	Warehouse.add (IT00003);
	Warehouse.add (IT00004);
	Warehouse.add (IT00005);
	Warehouse.add (IT00006);
	Warehouse.add (IT00007);
	Warehouse.add (IT00008);

	
	//Item i01 = Warehouse.getItem("IT00001");
	//i01.info();
	//i01 = Warehouse.getItem("IT00008");
	//i01.info();

	ArrayList<String> IDs = new ArrayList<String> ();
        IDs.add ("IT00003");

	System.out.println("item is " + Warehouse.getItem("IT00003").name + " and its price is " + Warehouse.getItem("IT00003").getPrice() );
	IDs.add ("IT00001");
	IDs.add ("IT00006");
	IDs.add ("IT00005");
	IDs.add ("IT00007");
	IDs.add ("IT00008");
	IDs.add ("IT00020");
	IDs.add ("IT00040");
	
	System.out.println();
	System.out.println();
	System.out.println("Shopping cart test(no-membership, no-tax-exempt):");
	
	ShoppingCart SC0001 = new ShoppingCart (Warehouse);
	SC0001.calcPurchasePrice(IDs,One);

	System.out.println();
	System.out.println();
	System.out.println("Shopping cart test(membership, no-tax-exempt):");
	One.setMembership(1);
	SC0001.calcPurchasePrice(IDs,One);

	System.out.println();
	System.out.println();
	System.out.println("Shopping cart test(no-membership, tax-exempt):");
	One.setMembership(0);	
	One.setTaxType(1);
	SC0001.calcPurchasePrice(IDs,One);

	System.out.println();
	System.out.println();
	System.out.println("Shopping cart test(membership, tax-exempt):");
	One.setMembership(1);
	SC0001.calcPurchasePrice(IDs,One);

	System.out.println();
	System.out.println();
	System.out.println("Shopping cart test if membership type is wrong:");
	
	One.setMembership(5);
	SC0001.calcPurchasePrice(IDs,One);


	System.out.println();
	System.out.println();
	System.out.println("Shopping cart test if tax-exempt type is wrong:");
			
	SC0001.calcPurchasePrice(IDs,One);

	System.out.println();
	System.out.println();
	System.out.println("Wrong price item in this list");

	Warehouse.add(new Item("IT00020", "dish", -3.4));
	ShoppingCart SC0002 = new ShoppingCart (Warehouse);
	One = new Customer ("Jun Wei", 0, 0);
	SC0001.calcPurchasePrice(IDs,One);

	}

}
