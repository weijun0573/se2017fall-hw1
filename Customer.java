import java.io.*;
import java.util.ArrayList;

//Class that define customer name and type.

public class Customer {

    //!Error flags
    /*still need to figure it out how to use enum
    enum Error {
	ERROR_NONE,                      //No Error
	ERROR_WRONGTYPE,                 //Type input Error
	ERROR_UNKNOWN			 //Catch-all for anything else
    }
    */

    public String name;
    public int membership;
    public int taxType;
        
    //empty constructor.
    public Customer(){} 

    //Using int to classify customer type.
    //Type of membership: 0, non-member; 1, member.
    //Type of taxType: 0, no-tax-exempt; 1, tax-exempt.
    //If input is not 0 or 1, then set type to -1.
    
    public Customer (String name, int membership, int taxType){
	this.name = name;
	if (membership == 0 || membership == 1)
	    this.membership = membership;
	else
	    this.membership = -1;
	if (taxType == 0 || taxType == 1)
	    this.taxType = taxType;
	else
	    this.taxType = -1;
    }

    //reset customer name
    public void setName(String name){
	this.name = name;
    }

    //get customer name
    public String getName(){
    	return name;
    }

    //reset costumer type of membership.
    public void setMembership(int membership){
        if (membership ==0 || membership ==1){
	    this.membership = membership;
	}
	else
	    this.membership = -1;
    }
	
    //get costumer type of membership.
    public int getMembership(){
	return membership;
    }


    //reset costumer type of tax-exempt.
    public void setTaxType(int taxType){
        if (taxType ==0 || taxType ==1){
	    this.taxType = taxType;
	}
	else
	    this.taxType = -1;
    }
	
    //get costumer type of tex-exmpt.
    public int getTaxType(){
	return taxType;
    }

    public void Info(){
	System.out.println("The name of this customer is " + name + ".");
	if (membership == 0)
	    System.out.println("This customer doesn't have membership.");
	else if (membership == 1)
	    System.out.println("This customer has membership.");
	else
	    System.out.println("Please reset membership type of this customer.");	
	if (taxType == 0)
	    System.out.println("This customer needs to pay tax.");
	else if (taxType == 1)
	    System.out.println("This customer does not have to pay tax.");
	else
	    System.out.println("Please reset tax type of this customer.");
    }


}
