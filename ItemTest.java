import static org.junit.Assert.*;
import org.junit.Test;
import java.lang.*;

public class ItemTest{
    
    double delta = 0.00001;
    /*
    //test empty constructor
    @Test
    public void evaluatesEmptyConstructor(){
	Item it0001 = new Item();
	assertEquals(null, it0001);
    }
    
    */

    //test constructor
    @Test
    public void evaluatesConstructor(){
	Item it0001 = new Item("0001", "soap", 5.43);
	assertEquals("0001", it0001.ID);
	assertEquals("soap", it0001.name);
	assertEquals(true, (Math.abs(it0001.price -5.43) < delta));
    }

    //test if the price initiation negative or 0
    @Test
    public void evaluateConstructor(){
	Item it0001 = new Item("0001", "soap", 0);
	assertEquals("0001", it0001.ID);
	assertEquals("soap", it0001.name);
	assertEquals(true, (Math.abs(it0001.price - (-1)) < delta));
	Item it0002 = new Item("0002", "dish", -0.01);
	assertEquals("0002", it0002.ID);
	assertEquals("dish", it0002.name);
	assertEquals(true, (Math.abs(it0002.price - (-1)) < delta));     
    }



    //test setName
    @Test
    public void evaluatSetName(){
	Item it0001 = new Item("0001", "soap", 5.43);
	it0001.setName ("dish");
	assertEquals("dish", it0001.name);
    }

    //test getName
    @Test
    public void evaluatGetName(){
	Item it0001 = new Item("0001", "soap", 5.43);
	assertEquals("soap", it0001.getName());
    }

    //test setPrice
    @Test
    public void evaluatSetPrice(){
	Item it0001 = new Item("0001", "soap", 5.43);
	it0001.setPrice (6.18);
	assertEquals(true, (Math.abs(it0001.price - 6.18) < delta));
    }

    //test getPrice
    @Test
    public void evaluatGetPrice(){
	Item it0001 = new Item("0001", "soap", 5.43);
	assertEquals(true, (Math.abs(it0001.getPrice()-5.43) < delta));
	it0001.setPrice(0);
	assertEquals(true, (Math.abs(it0001.getPrice() - (-1)) < delta));
	it0001.setPrice(6.18);
	assertEquals(true, (Math.abs(it0001.getPrice() - 6.18) < delta));
    }

}
