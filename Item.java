import java.io.*;

public class Item{

    String ID;
    String name;
    double price;

    //empty constructor.
    public Item(){}


    //if price less than 0, then set price to -1. (not sure if we need to set rounding model here or not)
    public Item(String ID, String name, double price){
	this.ID = ID;
	this.name = name;
	if (price <= 0)
		this.price = -1;
	else
		this.price = price;
    }

    public void setName(String name){
	this.name = name;
    }

    public String getName(){
	return name;
    }
 
    //re-set price. if new price is equal or less than 0, then set price to -1.
    public void setPrice(double newPrice){
        if (newPrice <= 0)
		price = -1;
	else
		price = newPrice;
    }

    public float getPrice(){
       return (float)price;
    }

}
