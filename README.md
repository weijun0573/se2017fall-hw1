# README #

This is the repository for the assignment 1 in the Spring 2017 session of Software Engineering (COMP 474) at Loyola U. Chicago. In this project I am using Java.

##How to run test?
All .java files all ready compiled into .class file. You can run Main file with:
$ java Main

To run test file, for example CustomerTest, you can use:

$java -cp .:junit-4.12.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore CustomerTest

For each class, I create a Test class.


## What is this repository for? ###

Here is the brief description of assignment 1:

Title	White-box testing and TDD

###Instructions
Task: Implement the following user story Implement the functionality incrementally using the test-drive development (TDD) principle. Create automated tests cases that provide statement, branch, conditional, and loop coverage as necessary. Create additional test cases using equivalence partitioning (EP) and boundary value analysis (BVA) techniques as needed. Keep a developer’s log documenting the increments coded, the associated test(s), what bugs were found by which test case, and why. Describe the intent and motivation for each test. 

User Story: Compute the net price of all the items in a customer’s shopping cart including all discounts and taxes.

After discussing further with the client, we have defined the following requirements / functionality for this story.

• If the shopping cart contains 10 or more items, the customers gets a 10% discount before tax. Otherwise, if the shopping cart contains more than 5 items, the customer gets a 5% discount before tax. The customer receives no discount if less than 5 items.

• If the customer is a member of the store’s discount shopping club, they get an additional 10% discount before taxes.

• Customers shall not purchase more than 50 items in a single shopping cart.

• The local sales tax rate is 4.5% but tax-exempt customers do not pay the sales tax.

• Dollar amounts shall be rounded to the nearest cent ($0.01) before and after the tax rate is applied.

• Report the net purchase price before any discounts, the net discount, the tax amount, and the net total due.

You are implementing only this story. 

calcPurchasePrice (StringList ProductIDs, Customer customer) which is a member of the ShoppingCart class. StringList is a list of strings (in your favorite OO language) that represent the item ID’s of the products in the customer’s shopping cart (assume they were scanned already) and Customer is a class with methods indicating if the customer is a member of the stores shopping club and if they are tax-exempt. I leave it to you and your partner to design of the Customer class and the list of strings parameters passed to the calcPurchasePrice method.

You will need to create a mock database object to supply information to the ShoppingCart class method(s).





### Who do I talk to? ###
Jun Wei (jwei4@luc.edu)

