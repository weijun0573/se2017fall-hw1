import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;
import java.lang.*;

public class ShoppingCartTest{

	double delta = 0.00001;


    @Test
    public void evaluateNumber50(){
	ShoppingCart sc = new ShoppingCart ();
	ArrayList<String> ProductIDs = new ArrayList<String>();
	for (int i = 0; i < 51; i++){
		int n = 101;
		ProductIDs.add(String.valueOf(n));
		n++;
	}
	assertEquals(sc.itemDiscount(ProductIDs), -1, delta);
    }

    //test item discount, if there are 10 <= n <=50 items in shopping cart
    @Test
    public void evaluateNumber10(){
	ShoppingCart sc = new ShoppingCart ();
	ArrayList<String> ProductIDs1 = new ArrayList<String>();
	for (int i = 0; i < 50; i++){
		int n = 101;
		ProductIDs1.add(String.valueOf(n));
		n++;
	}
	assertEquals(sc.itemDiscount(ProductIDs1), 0.1f, delta);
	ArrayList<String> ProductIDs2 = new ArrayList<String>();
	for (int i = 0; i < 10; i++){
		int n = 101;
		ProductIDs2.add(String.valueOf(n));
		n++;
	}
	assertEquals(sc.itemDiscount(ProductIDs2), 0.1f, delta);
    }

    //test item discount, if there are 5 < n < 10 items in shopping cart
    @Test
    public void evaluateNumber5(){
	ShoppingCart sc = new ShoppingCart ();
	ArrayList<String> ProductIDs1 = new ArrayList<String>();
	for (int i = 0; i < 9; i++){
		int n = 101;
		ProductIDs1.add(String.valueOf(n));
		n++;
	}
	assertEquals(sc.itemDiscount(ProductIDs1), 0.05f, delta);
	ArrayList<String> ProductIDs2 = new ArrayList<String>();
	for (int i = 0; i < 6; i++){
		int n = 101;
		ProductIDs2.add(String.valueOf(n));
		n++;
	}
	assertEquals(sc.itemDiscount(ProductIDs2), 0.05f, delta);
    }

    //test item discount, if there are <= 5 items in shopping cart
    @Test
    public void evaluateNumber1(){
	ShoppingCart sc = new ShoppingCart ();
	ArrayList<String> ProductIDs1 = new ArrayList<String>();
	for (int i = 0; i < 5; i++){
		int n = 101;
		ProductIDs1.add(String.valueOf(n));
		n++;
	}
	assertEquals(sc.itemDiscount(ProductIDs1), 0f, delta);
	ArrayList<String> ProductIDs2 = new ArrayList<String>();
	ProductIDs2.add("a");
	assertEquals(sc.itemDiscount(ProductIDs2), 0f, delta);
    }

    //test member discount
    @Test
    public void evaluateMembership(){
	ShoppingCart sc = new ShoppingCart ();
	Customer customer = new Customer ("Joe Doe", 0, 1);
	assertEquals(sc.memberDiscount(customer), 0f, delta);
	customer.setMembership(1);
	assertEquals(sc.memberDiscount(customer), 0.1f, delta);
	customer.setMembership(5);
	assertEquals(sc.memberDiscount(customer), -1.0f, delta);
    }

    //test tax rate
    @Test
    public void evaluateTaxRate(){
	ShoppingCart sc = new ShoppingCart ();
	Customer customer = new Customer ("Joe Doe", 1, 0);
	assertEquals(sc.taxRate(customer), 0.045f, delta);
	customer.setTaxType(1);
	assertEquals(sc.taxRate(customer), 0f, delta);
	customer.setTaxType(5);
	assertEquals(sc.taxRate(customer), -1.0f, delta);
    }

    //test null items in list
    @Test
    public void evaluateNullItem(){
	Item IT00001 = new Item ("IT00001", "soap", 1.01);
	Item IT00002 = new Item ("IT00002", "wine", 7.49);

	Items Warehouse = new Items();
	Warehouse.add (IT00001);
	Warehouse.add (IT00002);

	ShoppingCart sc = new ShoppingCart(Warehouse);

	ArrayList<String> ProductIDs = new ArrayList<String>();
	ProductIDs.add("IT00001");
	ProductIDs.add("IT00002");
	assertEquals(0, sc.nullItem(ProductIDs));
	ProductIDs.add("IT00020");
	assertEquals(1, sc.nullItem(ProductIDs));
	ProductIDs.add("IT00040");
	assertEquals(2, sc.nullItem(ProductIDs));
    }

    //test wrong price items in list
    @Test
    public void evaluateWrongPriceItem(){
	Item IT00001 = new Item ("IT00001", "soap", 1.01);
	Item IT00002 = new Item ("IT00002", "wine", 7.49);

	Items Warehouse = new Items();
	Warehouse.add (IT00001);
	Warehouse.add (IT00002);

	ShoppingCart sc = new ShoppingCart(Warehouse);

	ArrayList<String> ProductIDs = new ArrayList<String>();
	ProductIDs.add("IT00001");
	ProductIDs.add("IT00002");
	assertEquals(0, sc.wrongPriceItem(ProductIDs));
	
	Item IT00020 = new Item ("IT00020", "dish", -9.23);
	Warehouse.add (IT00020);
	ShoppingCart sc1 = new ShoppingCart(Warehouse);
	ProductIDs.add("IT00020");
	assertEquals(1, sc1.wrongPriceItem(ProductIDs));
	
    }



    //test purchase price in list
    @Test
    public void evaluatePurchasePrice(){

	Item IT00001 = new Item ("IT00001", "soap", 1.01);
	Item IT00002 = new Item ("IT00002", "wine", 7.49);

	Items Warehouse = new Items();
	Warehouse.add (IT00001);
	Warehouse.add (IT00002);

	ShoppingCart sc = new ShoppingCart(Warehouse);

	ArrayList<String> ProductIDs = new ArrayList<String>();
	ProductIDs.add("IT00001");
	ProductIDs.add("IT00002");
	float n1 = sc.netPurchasePrice(ProductIDs);
	float n2 = IT00001.getPrice() + IT00002.getPrice();
	assertEquals(true, (Math.abs(n1-n2) < delta));
	//Purchse price would not change if you add null items
	ProductIDs.add("IT00020");
	assertEquals(1, sc.nullItem(ProductIDs));
	ProductIDs.add("IT00040");
	float n3 = sc.netPurchasePrice(ProductIDs);
	assertEquals(true, (Math.abs(n3-n2) < delta));
    }

    //test rounding
    @Test
    public void evaluateRounding(){
	ShoppingCart sc = new ShoppingCart();
	assertEquals(true, (Math.abs(sc.rounding(0.789f) - 0.79f) < delta));
	assertEquals(true, (Math.abs(sc.rounding(0.78503f) - 0.79f) < delta));
	assertEquals(true, (Math.abs(sc.rounding(0.78499f) - 0.78f) < delta));
	assertEquals(true, (Math.abs(sc.rounding(0.78001f) - 0.78f) < delta));
	assertEquals(true, (Math.abs(sc.rounding(0.78f) - 0.78f) < delta));
    }


}
