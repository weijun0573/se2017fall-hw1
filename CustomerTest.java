import static org.junit.Assert.*;
import org.junit.Test;

public class CustomerTest{
    /*
    //test empty constructor
    @Test
    public void evaluatesEmptyConstructor(){
	Customer One = new Customer();
	assertEquals(null, One);
    }
    
    */

    //test constructor
    @Test
    public void evaluatesConstructor(){
	Customer One = new Customer("Jon Doe", 0, 0 );
	assertEquals("Jon Doe", One.name);
	assertEquals(0, One.membership);
	assertEquals(0, One.taxType);
    }

    //test setName
    @Test
    public void evaluatsetName(){
	Customer One = new Customer();
	One.setName ("Jon Doe");
	assertEquals("Jon Doe", One.getName());
    }


    //test get name
    @Test
    public void evaluatGetName(){
	Customer One = new Customer("Jon Doe", 0, 0);
	assertEquals("Jon Doe", One.getName());
	One.setName("Harry Potter");
	assertEquals("Harry Potter", One.name);

    }


    //test getMembership
    @Test
    public void evaluatesGetMembership(){
	Customer One = new Customer("Jon Doe", 0, 0 );
	int n = One.getMembership();
	assertEquals(0, n);
    }


    //if membership is not 0 or 1 in initiation, it should be changed to -1
    @Test
    public void evaluatesWrongMembership(){
	Customer One = new Customer("Jon Doe", 5, 0 );
	int n = One.getMembership();
	assertEquals(-1, n);
    }


    //test setMembership
    @Test
    public void evaluatsSetMembership(){
	Customer One = new Customer("Jon Doe", 0, 0);
	One.setMembership(1);
	int n = One.getMembership();
	assertEquals(1, n);
	//if setMembership is not 0 or 1, then it should be changed to -1
        One.setMembership(4);
	assertEquals(-1, One.membership);
    }

    //test getTaxType
    @Test
    public void evaluateGetTaxType(){
    	Customer One = new Customer ("Jon Doe", 0, 0);
	int n = One.getTaxType();
	assertEquals(0, n);
    }

    //test getTaxType if initiation is not 0 or 1
    @Test
    public void evaluateWrongTaxType(){
    	Customer One = new Customer ("Jon Doe", 0, 7);
	int n = One.getTaxType();
	assertEquals(-1,n); 
    }


    //test setTaxType
    @Test
    public void evaluateSetTaxType(){
    	Customer One = new Customer ("Jon Doe", 0, 0);
	One.setTaxType (1);
	int n = One.getTaxType();
	assertEquals(1,n); 
	One.setTaxType (30);
	assertEquals(-1, One.taxType);
    }

}
